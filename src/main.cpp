#include <iostream>
#include <iomanip>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "gnuplot-iostream.h"

#include "Utils.h"
#include "GaussEquationSolver.h"
#include "GaussSeidelEquationSolver.h"
#include "LinearEquation.h"
#include "LinearRegressionSolver.h"
#include "PolynomialRegressionSolver.h"
#include "NewtonInterpolationEquationSolver.h"
#include "CubicSplineSolver.h"
#include "KernelRegression.h"
#include "LinearEquationWithKernel.h"
#include "LinearEquation.h"
#include "MultipleLinearRegressionSolver.h"
#include "PowerKernel.h"

using namespace std;

int window_counter = 0;
void plotAndSave3dPoints(Gnuplot& gp, list<Point> inputPoints, string parentFolderPath, string fileName, string color, string title){
	gp << "set term x11 " + to_string(window_counter) << endl;
	window_counter++;
	gp << "set title '" + fileName + "-" + title + "'" << endl;
	//gp << "set xrange [-1:10]" << endl;
	//gp << "set yrange [-1:10]" << endl;
	//gp << " set hidden3d" << endl;
	gp << "splot '-' with lines title '" << title <<"' lt rgb '" << color << "' " << endl;
	gp.send2d(inputPoints);
	string filePath = parentFolderPath + fileName;
	string fileExtension = ".csv";
	gp << "set term png" << endl;
	gp<< "set output '" + filePath.replace(filePath.find(fileExtension), fileExtension.length(), + "_" + title +"_plot.png'") << endl;
	gp << "replot" << endl;
}
void solvePart2(string courseWorkFolder, int solver_type) {
	string parentFolderPath = courseWorkFolder + "part two datasets/";
	string fileNames[] = { "test.csv", "reg1.csv", "reg2.csv", "reg3.csv" };
	list<Point> inputPoints;
	string filePath;
	LinearRegressionSolver linearRegressionSolver;
	PolynomialRegressionSolver polynomialRegressionSolver(2);
	MultipleLinearRegressionSolver multiLinearSolver;
	KernelRegression kernelSolver;
	//Kernel* powerKernel = new PowerKernel(2);
	kernelSolver.setKernel(shared_ptr<Kernel>(new PowerKernel(2)));
	//PolynomialRegressionSolver polynomialRegressionSolver_3rd_degree(3);
	//PolynomialRegressionSolver polynomialRegressionSolver_4th_degree(4);

	Polynomial firstDegreePolynomial;
	Polynomial secondDegreePolynomial;
	LinearEquation multipleLinearRegressionEquation;
	LinearEquationWithKernel multiplePolynomialRegressionEquation;
	//Polynomial thirdDegreePolynomial;
	//Polynomial fourthDegreePolynomial;
	list<Point> interpolatedFirstDegreePolynomialPoints;
	list<Point> interpolatedSecondDegreePolynomialPoints;
	list<Point> interpolatedMultipleLinearRegressionPoints;
	list<Point> interpolatedKernelPolynomialPoints;
	list<double> x_coordinates;
	list<Point> multiDimesionalInputPoints;
	Gnuplot gp;
	double x;
	string fileExtension = ".csv";
	cout << "Question 2:" << endl;
	cout << "------------" << endl;
	bool is3rdFile;
	for (string fileName : fileNames) {
		is3rdFile = (fileName == fileNames[3]);
		cout << "File: " << fileName << endl;
		cout << "-------------------" << endl;
		filePath = parentFolderPath + fileName;
		inputPoints =
				(is3rdFile) ?
						Utils::loadPointsXdFromFile(filePath, 3) :
						Utils::loadPointsFromFile(filePath);
		if (is3rdFile) {
			multipleLinearRegressionEquation = multiLinearSolver.solve(
					inputPoints, solver_type);
			multiplePolynomialRegressionEquation = kernelSolver.solve(
					inputPoints, solver_type);
			cout << "Multiple Linear Regression equation: "
					<< multipleLinearRegressionEquation.getStringRepresentation()
					<< endl;
			cout << "Kernel Regression equation: "
					<< multiplePolynomialRegressionEquation.getStringRepresentation()
					<< endl;
			cout
								<< "---------------------------------------------------------------------------------------"
								<< endl;
			for(auto point:inputPoints){
				multiDimesionalInputPoints.push_back(Point(point.begin(), point.end() - 1));
			}
			interpolatedMultipleLinearRegressionPoints = multipleLinearRegressionEquation.evaluatePoints(multiDimesionalInputPoints, true);
			interpolatedKernelPolynomialPoints = multiplePolynomialRegressionEquation.evaluatePoints(multiDimesionalInputPoints, true);
			/*gp << "set term x11 " + to_string(window_counter) << endl;
			window_counter++;
			gp << "set title '" + fileName + "'" << endl;
			gp << "set xrange [-1:10]" << endl;
			gp << "set yrange [-1:10]" << endl;
			//gp << " set hidden3d" << endl;
			gp << "splot '-' with lines title 'original points' lt rgb 'green', ";
			gp << " '-' with lines title 'multi-linear regression' lt rgb 'red', ";
			gp << " '-' with lines title 'power 2 kernel regression' lt rgb 'blue'" << endl; //u 1:2:3
			gp.send2d(inputPoints);
			gp.send2d(interpolatedMultipleLinearRegressionPoints);
			gp.send2d(interpolatedKernelPolynomialPoints);*/
			plotAndSave3dPoints(gp, inputPoints, parentFolderPath, fileName, "black", "original points");
			plotAndSave3dPoints(gp, interpolatedMultipleLinearRegressionPoints, parentFolderPath, fileName, "green", "Multiple Linear Regression");
			plotAndSave3dPoints(gp, interpolatedKernelPolynomialPoints, parentFolderPath, fileName, "blue", "Kernel Regression");
			cout << "After Plotting" << endl;
		} else {
			firstDegreePolynomial = linearRegressionSolver.solve(inputPoints);
			secondDegreePolynomial = polynomialRegressionSolver.solve(
					inputPoints, solver_type);
			cout << "Linear Regression equation: "
					<< firstDegreePolynomial.get_string_representation()
					<< endl;
			cout << "Quadratic Degree Polynomial Regression equation: "
					<< secondDegreePolynomial.get_string_representation()
					<< endl;
			cout
					<< "---------------------------------------------------------------------------------------"
					<< endl;
			x_coordinates = list<double>();
			for (Point p: inputPoints) {
				//x = p[0];
				x = p[0];
				x_coordinates.push_back(x);
			}

			interpolatedFirstDegreePolynomialPoints =
					firstDegreePolynomial.evaluatePoints(x_coordinates);
			interpolatedSecondDegreePolynomialPoints =
					secondDegreePolynomial.evaluatePoints(x_coordinates);
			//interpolatedThirdDegreePolynomialPoints = thirdDegreePolynomial.evaluatePoints(x_coordinates);
			//interpolatedFouthDegreePolynomialPoints = fourthDegreePolynomial.evaluatePoints(x_coordinates);

			//Plotting on figure
			gp << "set term x11 " + to_string(window_counter) << endl;
			window_counter++;
			gp << "set title '" + fileName + "'" << endl; //will endl be good instead of \n
			gp << "plot '-' with points title 'original points' lt rgb 'green', ";
			gp << "'-' with lines title 'first order polynomial' lt rgb 'red', ";
			gp << "'-' with lines title 'second order polynomial' lt rgb 'blue'"
					<< endl;
			//gp << "'-' with lines title 'third order polynomial' lt rgb 'yellow', ";
			//gp << "'-' with lines title 'fouth order polynomial' lt rgb 'violet' " << endl; //will endl be good instead of \n
			gp.send1d(inputPoints);
			gp.send1d(interpolatedFirstDegreePolynomialPoints);
			gp.send1d(interpolatedSecondDegreePolynomialPoints);
			//gp.send1d(interpolatedThirdDegreePolynomialPoints);
			//gp.send1d(interpolatedFouthDegreePolynomialPoints);
			//plotting to a file
			gp << "set term png" << endl;
			gp<< "set output '" + filePath.replace(filePath.find(fileExtension), fileExtension.length(), "_plot.png'") << endl;
			gp << "replot" << endl;
		}
	}
}

void solvePart3(string courseWorkFolder, int solver_type) {
	string parentFolderPath = courseWorkFolder + "part three datasets/";
	string fileNames[] = { "test.csv", "test1.csv", "sp2.csv", "sp3.csv",
			"sp4.csv", "sp1-non-superious.csv", "sp1-superious.csv" };
	int resamplingFactors[] = { 2, 4, 8 };
	list<Point> inputPoints;
	string filePath;
	NewtonInterpolationEquationSolver newtonSolver;
	CubicSplineSolver cubicSplineSolver;
	NewtonPolynomial newtonPolynomial;
	Gnuplot gp;
	list<double> x_coordinates;
	list<double> x_coordinates_multiplied;
	list<Point> newtonEquationResult, cubicSplineResult;
	double x;
	string fileExtension = ".csv";
	list<list<Point>> linesAccumulator;
	for (string fileName : fileNames) {
		filePath = parentFolderPath + fileName;
		inputPoints = Utils::loadPointsFromFile(filePath);

		newtonPolynomial = newtonSolver.Solve(inputPoints);

		cubicSplineSolver = CubicSplineSolver(inputPoints);
		cubicSplineSolver.solve(solver_type);

		x_coordinates = list<double>();
		for (Point p: inputPoints) {
			//x = p[0];
			x = p[0];
			x_coordinates.push_back(x);
		}
		//opening a new window
		gp << "set term x11 " << to_string(window_counter) << endl;
		window_counter++;
		gp << "set title '" + fileName + "'" << endl;
		linesAccumulator = list<list<Point>>();
		gp << "plot ";
		for (int resampleFactor : resamplingFactors) {
			x_coordinates_multiplied = Utils::x_axis_multiplier(x_coordinates,
					resampleFactor);

			newtonEquationResult = newtonPolynomial.evaluatePoints(
					x_coordinates_multiplied);
			cubicSplineResult = cubicSplineSolver.interpolate(
					x_coordinates_multiplied);

			gp
					<< "'-' with lines title 'Newton "
							+ to_string(resampleFactor)
							+ "X polynomial' lt rgb 'red', ";
			//gp.send1d(newtonEquationResult);
			linesAccumulator.push_back(newtonEquationResult);

			gp
					<< "'-' with lines title 'Cubic Spline "
							+ to_string(resampleFactor)
							+ "X polynomial' lt rgb 'blue', ";
			//gp.send1d(cubicSplineResult);
			linesAccumulator.push_back(cubicSplineResult);
		}
		//Draw the original points as points in black over the plotted equations
		gp << "'-' with points title 'Original points' lt rgb 'black' " << endl;
		//gp.send1d(inputPoints);
		linesAccumulator.push_back(inputPoints);
		for (auto line : linesAccumulator) {
			gp.send1d(line);

		}
		//plotting to a file
		gp << "set term png" << endl;
		gp
				<< "set output '"
						+ filePath.replace(filePath.find(fileExtension),
								fileExtension.length(), "_plot.png'") << endl;
		gp << "replot" << endl;
	}
}

int main() {
	/*list<tuple<double, double>> testPointsForPlot;
	 for(double i = 0; i < 20; i++){
	 testPointsForPlot.push_back(tuple<double, double>(i, i * 2.1 + 3));
	 }
	 Utils::plotPoints(testPointsForPlot);*/
	string courseWorkFolder = "../coursework+1/";
	solvePart2(courseWorkFolder, SOLVER_TYPE_GAUSS);
	solvePart3(courseWorkFolder, SOLVER_TYPE_GAUSS);
	solvePart2(courseWorkFolder, SOLVER_TYPE_GAUSS_SEIDEL);
	solvePart3(courseWorkFolder, SOLVER_TYPE_GAUSS_SEIDEL);

	/*srand(time(NULL));

	 GaussEquationSolver solver3;
	 solver3.__dbgSetRandomSystem(4, 4);
	 solver3.__dbgPrintUnsolved("solver3");
	 solver3.solveEquations();*/

	//vector<vector<double>> augmented = {
	//	{2, 1, -1, 8},
	//	{-3, -1, 2, -11},
	// 	{-2, 1, 2, -3},
	//};
	//GaussEquationSolver solver;
	//solver.setFromAugmentedMatrix(augmented);
	//solver.solveEquations();
	//vector<double> unknowns = solver.getUnknowns();
	//cout << "x = " << endl;
	//for(int i = 0; i < unknowns.size(); i++) {
	//	cout << "  " << setprecision(5) << fixed << unknowns[i];
	//	cout << endl;
	//}
	//
	//vector<vector<double>> intermed = solver.asAugmentedMatrix();
	//GaussEquationSolver solver2;
	//solver2.setFromAugmentedMatrix(intermed);

	//solver.__dbgPrintUnsolved("solver1");
	//solver2.__dbgPrintUnsolved("solver2");

	//cout << "Hello World" << endl;
	return 0;
}

