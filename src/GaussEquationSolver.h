/*
 * GaussEquationSolver.h
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#ifndef GAUSSEQUATIONSOLVER_H_
#define GAUSSEQUATIONSOLVER_H_

#include <iostream>
#include <vector>
#include <map>

#include "GlobalMacros.h"
#include "LinearEquation.h"
#include "AbstractEquationSolver.h"
class AbstractEquationSolver;

using namespace std;
class GaussEquationSolver : public AbstractEquationSolver {
public:
	GaussEquationSolver(string title, vector<LinearEquation> equations = vector<LinearEquation>());
	virtual ~GaussEquationSolver();

	void backwardSubstitute(const vector<vector<double>> &matrix);
	virtual void solveEquations();
	void rowReduce(vector<vector<double>> &matrix,
			int start_row_idx, int start_col_idx);
};

#endif /* GAUSSEQUATIONSOLVER_H_ */
