/*
 * NewtonInterpolationEquationSolver.h
 *
 *  Created on: Nov 24, 2018
 *      Author: remon
 */

#ifndef NEWTONINTERPOLATIONEQUATIONSOLVER_H_
#define NEWTONINTERPOLATIONEQUATIONSOLVER_H_

#include "GlobalMacros.h"
#include "NewtonPolynomial.h"

class NewtonInterpolationEquationSolver {
public:
	NewtonInterpolationEquationSolver();
	virtual ~NewtonInterpolationEquationSolver();
	NewtonPolynomial Solve(list<Point> points);
};

#endif /* NEWTONINTERPOLATIONEQUATIONSOLVER_H_ */
