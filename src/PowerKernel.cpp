/*
 * PowerKernel.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#include "PowerKernel.h"
#include "Matrix.h"

PowerKernel::PowerKernel(int power):power(power) {
	// TODO Auto-generated constructor stub

}

PowerKernel::~PowerKernel() {
	// TODO Auto-generated destructor stub
}

Point PowerKernel::calculateKernel(Point input){
	this->representation = vector<string>();
	int numberOfDim = input.size();
	Matrix<double> powers_matrix(numberOfDim, this->power + 1, 1.0);
	Matrix<string> representationMatrix(numberOfDim, this->power + 1, "1");
	vector<double> currentRow;
	vector<string> currentRepresentationRow;
	for(int colIdx = 0; colIdx < numberOfDim; ++colIdx){
		currentRow = powers_matrix[colIdx];
		currentRepresentationRow = representationMatrix[colIdx];
		for(int rowIdx = 1; rowIdx <= this->power; ++rowIdx){
			powers_matrix[colIdx][rowIdx] = powers_matrix[colIdx][rowIdx - 1] * input[colIdx];
			representationMatrix[colIdx][rowIdx] = this->variableName + to_string(colIdx) + "^" + to_string(rowIdx);
		}
	}
	Point outputKernel, tmpKernel1, tmpKernel2;
	vector<string> representationKernel, tmpRepresentationKernel1, tmpRepresentationKernel2;
	for(int i = 0; i <= this->power; ++i){
		outputKernel.push_back(powers_matrix[0][i]);
		representationKernel.push_back(representationMatrix[0][i]);
	}
	//The following loops needs testing
	for(int dimIdx = 1; dimIdx < numberOfDim; ++dimIdx){
		tmpKernel1 = powers_matrix[dimIdx];
		tmpKernel2 = Point();
		tmpRepresentationKernel1 = representationMatrix[dimIdx];
		tmpRepresentationKernel2 = vector<string>();
		//tmpKernel2 = powers_matrix[dimIdx + 1];
		//outputKernel = Point();
		for(int outputKernelIdx = 0; outputKernelIdx < outputKernel.size(); ++outputKernelIdx){
			for(int tmpKernel1Idx = 0; tmpKernel1Idx < tmpKernel1.size(); ++tmpKernel1Idx){
				tmpKernel2.push_back(outputKernel[outputKernelIdx] * tmpKernel1[tmpKernel1Idx]);
				tmpRepresentationKernel2.push_back(representationKernel[outputKernelIdx] + "*" + tmpRepresentationKernel1[tmpKernel1Idx]);
			}
		}
		outputKernel = tmpKernel2;
		representationKernel = tmpRepresentationKernel2;
	}
	this->representation = representationKernel;
	return outputKernel;
}

vector<string> PowerKernel::getStringRepresentation(){
	return this->representation;
}
