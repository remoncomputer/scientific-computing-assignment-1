#include "GaussSeidelEquationSolver.h"
#include "GaussEquationSolver.h"
#include "Polynomial.h"
#include <vector>
#include <string>

using namespace std;
int main(int argc, char** argv) {

	GaussSeidelEquationSolver solver("Testing");

	vector<vector<double>> augmented = {
		{3, -0.1, -0.2, 7.85},
		{0.1, 7, -0.3, -19.3},
		{0.3, -0.2, 10, 71.4},
	};
	//vector<LinearEquation> equations;
	LinearEquation currentEquation;
	for(int i = 0; i < 3; i++){
		currentEquation = LinearEquation();
		for(int j = 0; j < 3; j++){
			currentEquation.addUnknown("a" + to_string(j));
			currentEquation.addCoefficient(augmented[i][j]);
		}
		currentEquation.setRightHandSide(augmented[i][3]);
		solver.addEquation(currentEquation);
	}
	//solver.setFromAugmentedMatrix(augmented);
	solver.solveEquations();

	vector<double> solution = solver.getSolutionSimple();

	cout << "Solution = " << endl;
	for(int i = 0; i < solution.size(); i++){
		cout << " " << solution[i] << endl;
	}
	return 0;
}

