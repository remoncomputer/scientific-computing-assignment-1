/*
 * NewtonPolynomial.cpp
 *
 *  Created on: Nov 24, 2018
 *      Author: remon
 */

#include "NewtonPolynomial.h"

#include <iterator>

NewtonPolynomial::NewtonPolynomial(list<double> interpolationXs): interpolationXs(interpolationXs) {
	// TODO Auto-generated constructor stub

}

NewtonPolynomial::~NewtonPolynomial() {
	// TODO Auto-generated destructor stub
}

Point NewtonPolynomial::evaluatePoint(double x){
	auto coefficientItr = coefficients.begin();
	double y_val = *coefficientItr++, x_accumulator = 1;
	auto interpolatingXsItr = interpolationXs.begin();
	for(; (coefficientItr != coefficients.end()) && (interpolatingXsItr != interpolationXs.end());
			++coefficientItr, ++interpolatingXsItr){
		x_accumulator *= x - *interpolatingXsItr;
		y_val += *coefficientItr * x_accumulator;
	}
	Point output;
	output.push_back(x);
	output.push_back(y_val);
	return output;
}
list<Point> NewtonPolynomial::evaluatePoints(list<double> x_coordinates){
	list<Point> outputPoints;

	double y_coordinate, x_accumulator;
	//vector<double>::iterator coefficientItr;
	//list<double>::iterator interpolatingXsItr;
	if(coefficients.size() != interpolationXs.size()){
		throw "Something isn't right see number of coefficients and number of interpolating Xs";
	}
	for(double x_coordinate: x_coordinates){
		/*auto coefficientItr = coefficients.begin();
		y_coordinate = *coefficientItr++;
		x_accumulator = 1;
		auto interpolatingXsItr = interpolationXs.begin();
		for(; (coefficientItr != coefficients.end()) && (interpolatingXsItr != interpolationXs.end());
				++coefficientItr, ++interpolatingXsItr){
			x_accumulator *= x_coordinate - *interpolatingXsItr;
			y_coordinate += *coefficientItr * x_accumulator;
		}
		outputPoints.push_back(Point(x_coordinate, y_coordinate));*/
		outputPoints.push_back(this->evaluatePoint(x_coordinate));
	}
	return outputPoints;
}
