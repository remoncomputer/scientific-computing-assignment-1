/*
 * MultipleLinearRegressionSolver.h
 *
 *  Created on: Dec 3, 2018
 *      Author: remon
 */

#ifndef MULTIPLELINEARREGRESSIONSOLVER_H_
#define MULTIPLELINEARREGRESSIONSOLVER_H_

#include "GlobalMacros.h"

#include "LinearEquation.h"
#include "LinearRegressionSolver.h"
#include <list>

class MultipleLinearRegressionSolver: public LinearRegressionSolver{
public:
	MultipleLinearRegressionSolver();
	virtual ~MultipleLinearRegressionSolver();
	LinearEquation solve(list<PointXd>, int solver_type);
};

#endif /* MULTIPLELINEARREGRESSIONSOLVER_H_ */
