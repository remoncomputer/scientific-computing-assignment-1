#include "CubicSplineSolver.h"
#include "GaussEquationSolver.h"
#include "GaussSeidelEquationSolver.h"
#include "AbstractEquationSolver.h"

CubicSplineSolver::CubicSplineSolver(list<Point> true_points){
	//sort the points by x_axis
	true_points.sort([](Point lhs, Point rhs) {
		return lhs[0] < rhs[0];
	});
	//save it to true points
	this->true_points = true_points;
}

const string CubicSplineSolver::polynomialUnknownNames[] = {"d", "c", "b", "a"};

Polynomial CubicSplineSolver::get_cubic_equation(int equation_num, double x_val, double right_hand_side) {
	Polynomial outputPolynomial(x_val);
	outputPolynomial.setRightHandSide(right_hand_side);
	for(string unkown: CubicSplineSolver::polynomialUnknownNames){
		outputPolynomial.addUnknown(unkown + to_string(equation_num));
		outputPolynomial.addCoefficient(1.0);
	}
	outputPolynomial.prepareUnderlingEquationForSolver();
	return outputPolynomial;
}
 void CubicSplineSolver::getPolynomialFromItrAtPoint(Point& p, int equationIdx, bool calculateDerivatives,
		 Polynomial& outPolynomial, Polynomial& firstDerivative, Polynomial& secondDerivative){
	double x_val = p[0];
	double y_val = p[1];
	outPolynomial = this->get_cubic_equation(equationIdx, x_val, y_val);
	if(calculateDerivatives){
		firstDerivative = outPolynomial.diffrentiate();
		secondDerivative = firstDerivative.diffrentiate();
	}
}
void CubicSplineSolver::solve(int solver_type){
	list<Polynomial> equations;
	Polynomial currentPolynomialAtCurrentPoint, currentPolynomialAtNextPoint;
	int counter = 0;
	int num_points = this->true_points.size();
	//vector<point> true_points _vec...initialize
	//point current_point;
	//point next_point;
	double x_val, y_val, next_x_val, next_y_val;
	Polynomial currentFirstDerivativeAtNextPoint, currentSecondDerivativeAtNextPoint,
				nextFirstDerivativeAtNextPoint, nextSecondDerivativeAtNextPoint, nextPolynomialAtNextPoint,
				diffFirstDerivative, diffSecondDerivative, dummy;
	Point currentPoint, nextPoint;
	//int first_derivative_counter = 0, second_derivative_counter = 0;
	//int end_points_derivative_counter = 0, equations_substitution_counters = 0;
	auto pointItr = this->true_points.begin();
	getPolynomialFromItrAtPoint(*pointItr, counter, true,
			nextPolynomialAtNextPoint, nextFirstDerivativeAtNextPoint, nextSecondDerivativeAtNextPoint);
	equations.push_back(nextSecondDerivativeAtNextPoint);
	for(; counter < (num_points - 2); counter++){
		/*currentPoint = *pointItr;
		x_val = get<0>(currentPoint);
		y_val = get<1>(currentPoint);
		currentPolynomialAtCurrentPoint = this->get_cubic_equation(counter, x_val, y_val);*/
		getPolynomialFromItrAtPoint(*pointItr++, counter, false,
				currentPolynomialAtCurrentPoint, dummy, dummy);
		equations.push_back(currentPolynomialAtCurrentPoint);
		//equations_substitution_counters++;
		//++pointItr;
		/*nextPoint = *pointItr;
		next_x_val = get<0>(nextPoint);
		next_y_val = get<1>(nextPoint);
		currentPolynomialAtNextPoint = this->get_cubic_equation(counter, next_x_val, next_y_val);*/
		getPolynomialFromItrAtPoint(*pointItr, counter, true,
				currentPolynomialAtNextPoint, currentFirstDerivativeAtNextPoint, currentSecondDerivativeAtNextPoint);
		equations.push_back(currentPolynomialAtNextPoint);
		//if(1 <= counter && counter < num_points){
		//diffrentiate current eq 1st derivative
		//currentFirstDerivativeAtNextPoint = currentPolynomialAtNextPoint.diffrentiate();
		//get second derivative
		//currentSecondDerivativeAtNextPoint = currentFirstDerivativeAtNextPoint.diffrentiate();
		//make next equation with next point
		//nextPoint = *(++pointItr);
		//next_x_val = get<0>(nextPoint);
		//next_y_val = get<1>(nextPoint);
		//nextPolynomialAtNextPoint = this->get_cubic_equation(counter + 1 , next_x_val, next_y_val);
		//diffrentiate 1st derivative
		//nextFirstDerivativeAtNextPoint = nextPolynomialAtNextPoint.diffrentiate();
		//diffrentiate 2nd derivative
		//nextSecondDerivativeAtNextPoint = nextFirstDerivativeAtNextPoint.diffrentiate();
		//1st derivate final = diff of 1st derivative
		getPolynomialFromItrAtPoint(*pointItr, counter + 1, true,
				dummy, nextFirstDerivativeAtNextPoint, nextSecondDerivativeAtNextPoint);
		diffFirstDerivative = nextFirstDerivativeAtNextPoint - currentFirstDerivativeAtNextPoint;
		//2nd derivative final = diff of 2d derivate
		diffSecondDerivative = nextSecondDerivativeAtNextPoint - currentSecondDerivativeAtNextPoint;
		equations.push_back(diffFirstDerivative);
		//first_derivative_counter++;
		equations.push_back(diffSecondDerivative);
		//second_derivative_counter++;
		//}
		/*if(counter == 1 || counter == num_points){
			//at the final point
			currentFirstDerivative = current_pol.diffrentiate();
			currentSecondDerivative = currentFirstDerivative.diffrentiate();
			equations.push_back(currentSecondDerivative);
			end_points_derivative_counter++;
		}*/
	}
	/*currentPoint = *pointItr;
	++pointItr;
	x_val = get<0>(currentPoint);
	y_val = get<1>(currentPoint);
	currentPolynomialAtCurrentPoint = this->get_cubic_equation(counter, x_val, y_val);
	equations.push_back(currentPolynomialAtCurrentPoint);*/
	getPolynomialFromItrAtPoint(*pointItr++, counter, false,
			currentPolynomialAtCurrentPoint, dummy, dummy);
	equations.push_back(currentPolynomialAtCurrentPoint);

	/*nextPoint = *pointItr; //The final point
	next_x_val = get<0>(nextPoint);
	next_y_val = get<1>(nextPoint);
	currentPolynomialAtNextPoint = this->get_cubic_equation(counter, next_x_val, next_y_val);
	equations.push_back(currentPolynomialAtNextPoint);*/
	getPolynomialFromItrAtPoint(*pointItr, counter, true,
			currentPolynomialAtNextPoint, currentFirstDerivativeAtNextPoint, currentSecondDerivativeAtNextPoint);
	equations.push_back(currentPolynomialAtNextPoint);
	equations.push_back(currentSecondDerivativeAtNextPoint);
	//cout << first_derivative_counter << "," << second_derivative_counter << ",";
	//cout << end_points_derivative_counter << "," << equations_substitution_counters << endl;
	//cout << equations.size() << endl;
	//Util::prepare_for_solver(equations); //This should be handled automatically by the solver
	
	// create empty solvers
	GaussEquationSolver gauss_solver("Cubic Spline Problem");
	GaussSeidelEquationSolver gauss_seidel_solver("Cubic Spline Problem");

	// choose one of them
	AbstractEquationSolver *solver;
	if(solver_type == SOLVER_TYPE_GAUSS) {
		solver = &gauss_solver;
	}
	else if(solver_type == SOLVER_TYPE_GAUSS_SEIDEL) {
		solver = &gauss_seidel_solver;
	}
	solver->addEquations(vector<LinearEquation>(equations.begin(), equations.end()));

	//solve equations
	solver->solveEquations();
	//getsolution
	map<string, double> solution = solver->getSolution();

	//map<string, double> unknownMap get from solver
	int num_equations = num_points - 1;
	//list<Polynomial> final_equations;
	interpolation_region currentInterpolationRegion;
	string currentCoefficientName;
	double currentCoefficientValue;
	Polynomial currentCubicEquation;
	//string cofficientNames[] = CubicSplineSolver::polynomialUnknownNames;
	auto true_points_itr = this->true_points.begin();
	//Point currentPoint, nextPoint;
	for(int equation_idx = 0; equation_idx < num_equations; equation_idx++){
		currentInterpolationRegion = interpolation_region();
		currentCubicEquation = Polynomial();
		for(string coefficientName: CubicSplineSolver::polynomialUnknownNames){
			currentCoefficientName = coefficientName + to_string(equation_idx);
			currentCoefficientValue = solution[currentCoefficientName];
			currentInterpolationRegion.equation.addCoefficient(currentCoefficientValue);
		}
		currentPoint = *true_points_itr++;
		currentInterpolationRegion.lower_boundary = currentPoint[0];
		nextPoint = *true_points_itr;
		currentInterpolationRegion.upper_boundary = nextPoint[0];
		this->regions.push_back(currentInterpolationRegion);
	}
}

	list<Point> CubicSplineSolver::interpolate(list<double> x_coordinates){
		x_coordinates.sort([](double lhs, double rhs) {
			return lhs < rhs;
		}); //be sure that the corrdinates are sorted in ascending order

		list<Point> outputPoints;
		//Polynomial current_equation;
		auto x_itr = x_coordinates.begin();
		double x_val;
		Point currentPoint;
		for(auto region_itr = this-> regions.begin();
				region_itr != this-> regions.end() && x_itr !=x_coordinates.end(); ++x_itr){
			x_val = *x_itr;
			for(; region_itr != this-> regions.end(); ++region_itr){
				if(x_val >= region_itr->lower_boundary && x_val <= region_itr->upper_boundary){
					break;
				}
			}
			currentPoint = region_itr->equation.evaluatePoint(x_val);
			outputPoints.push_back(currentPoint);
		}
		return outputPoints;
	}
