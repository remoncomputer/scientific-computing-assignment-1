/*
 * PowerKernel.h
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#ifndef POWERKERNEL_H_
#define POWERKERNEL_H_

#include "Kernel.h"

class PowerKernel: public Kernel {
private:
	int power;
	vector<string> representation;
	const string variableName = "X";
public:
	PowerKernel(int power=1);
	virtual ~PowerKernel();
	virtual Point calculateKernel(Point);
	virtual vector<string> getStringRepresentation();
};

#endif /* POWERKERNEL_H_ */
