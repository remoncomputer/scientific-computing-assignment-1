#pragma once
#include "GlobalMacros.h"
#include "Polynomial.h"

#include <list>

class CubicSplineSolver{	
	private:
		list<Point> true_points;
		struct interpolation_region{
			double lower_boundary;
			double upper_boundary;
			Polynomial equation;
		};
		list<interpolation_region> regions;
		const static string polynomialUnknownNames[];
	public:
		CubicSplineSolver(list<Point> true_points = list<Point>());
		void solve(int solver_type);
		list<Point> interpolate(list<double>);
		Polynomial get_cubic_equation(int equation_idx, double x_val, double right_hand_side);
		void getPolynomialFromItrAtPoint(Point& p, int equationIdx, bool calculateDerivatives,
				 Polynomial& outPolynomial, Polynomial& firstDerivative, Polynomial& secondDerivative);
};
