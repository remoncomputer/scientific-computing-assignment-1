/*
 * MultiplePolynomialRegression.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#include "KernelRegression.h"
#include "Matrix.h"
#include "LinearEquation.h"
#include "GaussSeidelEquationSolver.h"

KernelRegression::KernelRegression() {
	// TODO Auto-generated constructor stub

}

KernelRegression::~KernelRegression() {
	// TODO Auto-generated destructor stub
}

LinearEquationWithKernel KernelRegression::solve(list<Point> points, int solver_type){
	list<Point> inputToKernel;
	//Point currentPoint;
	for(auto currentPoint: points){
		inputToKernel.push_back(Point(currentPoint.begin(), --currentPoint.end())); //will this work
	}
	list<Point> kernelValues = this->kernel->calculateKernel(inputToKernel);
	int numPoints = points.size();
	int kernalDim = kernelValues.begin()->size();
	Matrix<double> T(numPoints, kernalDim, 0);//This is the T matrix (Look Ref 1 p.g 4 eq 12)
	int colIdx = 0, rowIdx = 0;
	for(auto kernelValue:kernelValues){
		rowIdx = 0;
		for(auto coordinateValue:kernelValue){
			T[colIdx][rowIdx] = coordinateValue;
			rowIdx++;
		}
		colIdx++;
	}
	Matrix<double> Y(numPoints, 1, 0);
	auto pointsItr = points.begin();
	for(colIdx = 0; colIdx < points.size() && pointsItr != points.end(); ++colIdx, ++pointsItr){
		Y[colIdx][0] = *(--pointsItr->end());
	}
	Matrix<double> T_transpose = T.transpose();
	Matrix<double> A = T_transpose * T ;//This is the T matrix (Look Ref 1 p.g 4 eq 12)
	Matrix<double> C = T_transpose * Y;//This is C (Look Ref 1 p.g 4 eq 11)
	vector<LinearEquation> equations;
	string unknownSuffixName = "a";
	LinearEquation currentEquation;
	for(colIdx = 0; colIdx < kernalDim; ++colIdx){
		currentEquation = LinearEquation();
		for(rowIdx = 0; rowIdx < kernalDim; ++rowIdx){
			currentEquation.addUnknown(unknownSuffixName + to_string(rowIdx));
			currentEquation.addCoefficient(A[colIdx][rowIdx]);
		}
		currentEquation.setRightHandSide(C[colIdx][0]);
		equations.push_back(currentEquation);
	}

	// create empty solvers
	GaussEquationSolver gauss_solver("Kernel Regression Problem");
	GaussSeidelEquationSolver gauss_seidel_solver("Kernel Regression Problem");

	// choose one of them
	AbstractEquationSolver *solver;
	if(solver_type == SOLVER_TYPE_GAUSS) {
		solver = &gauss_solver;
	}
	else if(solver_type == SOLVER_TYPE_GAUSS_SEIDEL) {
		solver = &gauss_seidel_solver;
	}
	solver->addEquations(equations);

	solver->solveEquations();
	map<string, double> solution = solver->getSolution();
	LinearEquationWithKernel outputEquation;
	outputEquation.setKernel(this->kernel);
	for(int i = 0; i < kernalDim; i++){
		outputEquation.addCoefficient(solution[unknownSuffixName + to_string(i)]);
	}
	return outputEquation;
}
