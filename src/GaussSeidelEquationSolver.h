/*
 * GaussSeidelEquationSolver.h
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#ifndef GAUSSSEIDELEQUATIONSOLVER_H_
#define GAUSSSEIDELEQUATIONSOLVER_H_

#include <iostream>
#include <vector>

#include "LinearEquation.h"
#include "AbstractEquationSolver.h"

using namespace std;
class GaussSeidelEquationSolver : public AbstractEquationSolver {

public:
	GaussSeidelEquationSolver(string title, vector<LinearEquation> equations = vector<LinearEquation>());
	virtual ~GaussSeidelEquationSolver();

	vector<vector<double>> asAugmentedMatrixSimple();

	vector<double> getSolutionSimple();

	virtual void solveEquations();
};

#endif /* GAUSSSEIDELEQUATIONSOLVER_H_ */
