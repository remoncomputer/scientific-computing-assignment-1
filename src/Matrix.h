/*
 * Matrix.h
 *
 *  Created on: Nov 24, 2018
 *      Author: remon
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include "Utils.h"

#include <iostream>
#include <stdexcept>

using namespace std;

template <class T>
class Matrix {
private:
	vector<vector<T>> values;
	int height;
	int width;
public:
	Matrix(int height, int width, T initialValue): height(height), width(width) {
		/*values = new T*[height];
		for(int i = 0; i < height; i++){
			Utils::initializeArray(values[i], width, initialValue);
		}*/
		vector<T> currentRow;
		for(int colIdx = 0; colIdx < height; ++colIdx){
			currentRow = vector<T>();
			for(int rowIdx = 0; rowIdx < width; ++rowIdx){
				currentRow.push_back(initialValue);
			}
			values.push_back(currentRow);
		}
	}

	virtual ~Matrix(){
		/*for(int i = 0; i < height; i++){
			delete[] values[i];
		}
		delete[] values;
		values = NULL;*/
	}
	T get(int rowIdx, int colIdx) {return values[rowIdx][colIdx];}

	void set(int rowIdx, int colIdx, T value) {values[rowIdx][colIdx] = value;}

	vector<T> operator [](int i) const    {return values[i];}

	vector<T> & operator [](int i) {return values[i];}

	Matrix<T> operator*(Matrix<T> secondMatrix){
		int outputHeight = this->height;
		int outWidth = secondMatrix.width;
		int commonDim = this->width;
		if(this->width != secondMatrix.height){
			throw std::invalid_argument("Invalid Matrices for multiplication");
		}
		Matrix<T> output(outputHeight, outWidth, 0);
		for(int i = 0; i < outputHeight; ++i){
			for(int j = 0; j < outWidth; ++j){
				for(int k = 0; k < commonDim; ++k){
					output[i][j] += (*this)[i][k] * secondMatrix[k][j];
				}
			}
		}
		return output;
	}

	Matrix<T> transpose(){
		Matrix<T> output(width, height , 0);
		for(int i = 0; i < this->height; ++i){
			for(int j = 0; j < this->width; ++j){
				output[j][i] = (*this)[i][j];
			}
		}

		return output;
	}
};

#endif /* MATRIX_H_ */
