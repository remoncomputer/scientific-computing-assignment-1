/*
 * LinearRegressionSolver.h
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#ifndef LINEARREGRESSIONSOLVER_H_
#define LINEARREGRESSIONSOLVER_H_
#include "Polynomial.h"
#include "GlobalMacros.h"

#include <tuple>
#include <list>



using namespace std;

class LinearRegressionSolver {
public:
	LinearRegressionSolver();
	virtual ~LinearRegressionSolver();
	Polynomial solve(list<Point>);
};

#endif /* LINEARREGRESSIONSOLVER_H_ */
