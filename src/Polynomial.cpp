/*
 * Polynomial.cpp
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#include <type_traits>

#include "Polynomial.h"

Polynomial::Polynomial(double x): x(x), equation_ready_for_solver(false) {
	// TODO Auto-generated constructor stub

}

Polynomial::Polynomial(LinearEquation equation){
	this->coefficients = equation.get_coefficients();
	this->featureNames = equation.get_featureNames();
	this->features = equation.get_features();
	this->right_hand_side = equation.get_right_hand_side();
	this->x = 0;
	this->equation_ready_for_solver = true;
}

Polynomial::~Polynomial() {
	// TODO Auto-generated destructor stub
}

void Polynomial::evaluateRightHandSide(){
	this->right_hand_side = 0;
	double x_power_n_accumulator = 1;
	for(double coefficient:this->coefficients){//auto it = this->coefficients.begin(); it != this->coefficients.end(); it++
		this->right_hand_side += coefficient * x_power_n_accumulator;
		x_power_n_accumulator *= this->x;
	}
}

void Polynomial::setx(double){
	this->x = x;
}

Point Polynomial::evaluatePoint(double x_val){
	this->x = x_val;
	double y_val = 0;
	double x_power_n_accumulator = 1;
	for(double coefficient:this->coefficients){//auto it = this->coefficients.begin(); it != this->coefficients.end(); it++
		y_val += coefficient * x_power_n_accumulator;
		x_power_n_accumulator *= x_val;
	}
	cout << "Ploynomial Point: " << x_val << "," << y_val << endl;
	Point output;
	output.push_back(x_val);
	output.push_back(y_val);
	return output;
}
list<Point> Polynomial::evaluatePoints(list<double> x_coordinates){
	list<Point> points;
	for(double x_val: x_coordinates){
		//auto features_begin_itr = this->coefficients.begin();
		//double y_val = *features_begin_itr++;
		/*double y_val = 0;
		double x_power_n_accumulator = 1;
		for(double coefficient:this->coefficients){//auto it = this->coefficients.begin(); it != this->coefficients.end(); it++
			y_val += coefficient * x_power_n_accumulator;
			x_power_n_accumulator *= x_val;
		}*/
		//points.push_back(tuple<double, double>(x_val, y_val));
		points.push_back(this->evaluatePoint(x_val));
	}
	return points;
}

Polynomial Polynomial::diffrentiate(){
	Polynomial derivativeEquation(this->x);
	derivativeEquation.coefficients = vector<double>(this->coefficients.begin() + 1, this->coefficients.end());
	int power = 1;
	for(auto coefficientItr = derivativeEquation.coefficients.begin();
			coefficientItr != derivativeEquation.coefficients.end(); ++coefficientItr){
		*coefficientItr *= power++;
	}
	derivativeEquation.featureNames = vector<string>(this->featureNames.begin() + 1, this->featureNames.end());
	derivativeEquation.equation_ready_for_solver = this->equation_ready_for_solver;
	derivativeEquation.right_hand_side = 0;
	if(this->features.size() > 0){
		derivativeEquation.features = vector<double>(this->features.begin() + 1, this->features.end());
	}
	return derivativeEquation;
}

void Polynomial::prepareUnderlingEquationForSolver(){
	if(this->equation_ready_for_solver == false){
		double x_accumulator = 1;

		for(auto coefficientItr = this->coefficients.begin(); coefficientItr != this->coefficients.end(); ++coefficientItr){
			*coefficientItr *= x_accumulator;
			x_accumulator *= this->x;
		}

		this->equation_ready_for_solver = true;
	}
}

string Polynomial::get_string_representation(){
	string output = to_string(this->coefficients[0]);
	for(int idx = 1; idx < this->coefficients.size(); ++idx){
		output += " + " + to_string(this->coefficients[idx]) + "*x^" + to_string(idx);
	}
	return output;
}

Polynomial Polynomial::operator-(Polynomial& secondEquation){

	LinearEquation firstArg = ((LinearEquation)*this);
	LinearEquation secondArg = (LinearEquation)secondEquation;
	Polynomial output =  secondArg - firstArg;
	if(this->x != secondEquation.x){
		throw "The x member of the  difference between two equations must be equal";
	}
	if(this->equation_ready_for_solver != secondEquation.equation_ready_for_solver){
		throw "The equation_ready_for_solver member of the  difference between two equations must be equal";
	}
	//static_assert(this->x == secondEquation.x, "The x member of the  difference between two equations must be equal");
	//static_assert(this->equation_ready_for_solver == secondEquation.equation_ready_for_solver, "The equation_ready_for_solver member of the  diffrence between two equations must be equal");
	output.x = this->x;
	output.equation_ready_for_solver = this->equation_ready_for_solver;
	return output;
}
