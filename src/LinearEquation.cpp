/*
 * LinearEquation.cpp
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#include <time.h>
#include <math.h>
#include <iomanip>

#include "LinearEquation.h"

LinearEquation::LinearEquation(vector<double> coefficients, vector<double> features, double right_hand_side_value)
{
	this->coefficients = coefficients;
	this->features = features;
	this->right_hand_side = right_hand_side_value;
}

LinearEquation::~LinearEquation() {
	// TODO Auto-generated destructor stub
}

void LinearEquation::addCoefficient(double cofficient){
	coefficients.push_back(cofficient);
}
void LinearEquation::addFeature(double feature){
	features.push_back(feature);
}

void LinearEquation::addUnknown(string name){
	this->featureNames.push_back(name);
}

void LinearEquation::setCoefficient(double value, int idx){
	if(idx >= coefficients.size()){
			throw std::runtime_error("Coefficient index out of range of available features");
		}
		coefficients[idx] = value;
}

void LinearEquation::setFeature(double value, int idx){
	if(idx >= features.size()){
			throw std::runtime_error("Feature index out of range of available features");
		}
		features[idx] = value;
}

double LinearEquation::getCoefficient(int idx){
	if(idx >= coefficients.size()){
		throw std::runtime_error("Coefficient index out of range of available features");
	}
	return coefficients[idx];
}
double LinearEquation::getFeature(int idx){
	if(idx >= features.size()){
		throw std::runtime_error("Feature index out of range of available features");
	}
	return features[idx];
}

double LinearEquation::getFeatureCoefficientByName(string name){
	for(int i = 0; i < this->featureNames.size(); i++){
		if(name == this->featureNames[i]){
			return this->coefficients[i];
		}
	}
	return 0;
}
double LinearEquation::getRightHandSide(){
	return right_hand_side;
}

void LinearEquation::setRightHandSide(double value){
	right_hand_side = value;
}

double LinearEquation::getErrorSquared() {
	double temp = evaluateLeftHandSide() - right_hand_side;
	return temp * temp;
}

double LinearEquation::evaluateLeftHandSide(){
	double result = 0;

	for(int i=0; i < coefficients.size(); i++){
		result += coefficients[i] * features[i];
	}

	return result;
}

void LinearEquation::evaluateRightHandSide(){
	right_hand_side = 0;
	if(coefficients.size() != features.size()){
		throw std::runtime_error("Features and coefficients must be the same size for evaluating the right hand side");
	}
	for(int i=0; i < coefficients.size(); i++){
		right_hand_side += coefficients[i] * features[i];
	}
}

string LinearEquation::getStringRepresentation(){
	string output = "";
	auto coeffientsItr = this->coefficients.begin();
	auto featureNamesItr = this->featureNames.begin();
	string sign ;//= (*coeffientsItr >= 0)? "":"-";
	output += to_string(*coeffientsItr) + " * " + *featureNamesItr;
	++coeffientsItr, ++featureNamesItr;
	for(; coeffientsItr != this->coefficients.end() && featureNamesItr != this->featureNames.end();
			++coeffientsItr, ++featureNamesItr){
		string sign = (*coeffientsItr >= 0)? " + ":" ";
		output += sign + to_string(*coeffientsItr) + " * " + *featureNamesItr;
	}
	return output;
}
PointXd LinearEquation::evaluatePoint(PointXd p, bool addbias){
	if(addbias){
		PointXd newPoint = Point();
		newPoint.push_back(1);
		for(auto coordinateValue:p){
			newPoint.push_back(coordinateValue);
		}
		this->features = newPoint;
	}else{
		this->features = p;
	}
	this->evaluateRightHandSide();
	PointXd output(p.begin(), p.end());
	output.push_back(this->right_hand_side);
	return output;
}

list<PointXd> LinearEquation::evaluatePoints(list<PointXd> inputPoints, bool addbias){
	list<PointXd> outputPoints;
	for(auto p:inputPoints){
		outputPoints.push_back(this->evaluatePoint(p, addbias));
	}
	return outputPoints;
}
/*LinearEquation LinearEquation::operator-(const LinearEquation& secondEquation){
	LinearEquation output;
	output.right_hand_side = this->right_hand_side - secondEquation.right_hand_side;

	//we are assuming that the unknowns of the first equation is different from the unknowns of the second equation
	output.featureNames = vector<string>(this->featureNames.begin(), this->featureNames.end());
	output.featureNames.insert(output.featureNames.end(), secondEquation.featureNames.begin(), secondEquation.featureNames.end());

	output.features = vector<double>(this->features.begin(), this->features.end());
	output.features.insert(output.features.end(), secondEquation.features.begin(), secondEquation.features.end());

	output.coefficients = vector<double>(this->coefficients.begin(), this->coefficients.end());
	for(double coefficient: secondEquation.coefficients){
		output.coefficients.push_back(-coefficient);
	}

	return output;
}*/

LinearEquation operator-(const LinearEquation& firstArg, const LinearEquation& secondArg){
	LinearEquation output;
	output.right_hand_side = firstArg.right_hand_side - secondArg.right_hand_side;

	//we are assuming that the unknowns of the first equation is different from the unknowns of the second equation
	output.featureNames = vector<string>(firstArg.featureNames.begin(), firstArg.featureNames.end());
	output.featureNames.insert(output.featureNames.end(), secondArg.featureNames.begin(), secondArg.featureNames.end());

	output.features = vector<double>(firstArg.features.begin(), firstArg.features.end());
	output.features.insert(output.features.end(), secondArg.features.begin(), secondArg.features.end());

	output.coefficients = vector<double>(firstArg.coefficients.begin(), firstArg.coefficients.end());
	for(double coefficient: secondArg.coefficients){
		output.coefficients.push_back(-coefficient);
	}

	return output;
}

void LinearEquation::setFromAugmentedMatrixRow(
		const vector<vector<double>> &matrix, int row_idx){

	int col_idx = 0;

	while(col_idx < matrix[row_idx].size() - 1) {
		coefficients.push_back(matrix[row_idx][col_idx]);
		col_idx++;
	}

	right_hand_side = matrix[row_idx][col_idx];
}

vector<double> LinearEquation::asAugmentedMatrixRowSimple() {
	vector<double> row;
	for(int i = 0; i < coefficients.size(); i++) {
		row.push_back(coefficients[i]);
	}
	row.push_back(right_hand_side);
	return row;
}

vector<double> LinearEquation::asAugmentedMatrixRow(map<string, int> unknownIndices){
	vector<double> row(unknownIndices.size() + 1);

	/*for(int i = 0; i < coefficients.size(); i++) {
		row.push_back(coefficients[i]);
	}*/
	double featureCoefficient;
	string featureName;
	int featureIndex;
	for(auto entry: unknownIndices){
		featureName = entry.first;
		featureIndex = entry.second;
		featureCoefficient = this->getFeatureCoefficientByName(featureName);
		row[featureIndex] = featureCoefficient;
	}

	//row.push_back(right_hand_side);
	row[unknownIndices.size()] = right_hand_side;

	return row;
}

void LinearEquation::__dbgPrintUnsolved(){
	for(int i = 0; i < coefficients.size(); i++) {

		if(i != 0 && coefficients[i] >= -0) {
			cout << "\t+";
		}
		else {
			cout << "\t";
		}

		cout << setprecision (5) << fixed << coefficients[i] << "y" << (coefficients.size() - i);
	}

	cout << "\t= " << right_hand_side << endl;
}

void LinearEquation::__dbgSetRandomCoefficients(int features_count){

	for(int i = 0; i < features_count; i++) {
		coefficients.push_back((rand() % 20) - 10);
	}
	right_hand_side = (rand() % 20) - 10;
}

