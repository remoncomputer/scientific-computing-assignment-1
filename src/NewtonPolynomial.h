/*
 * NewtonPolynomial.h
 *
 *  Created on: Nov 24, 2018
 *      Author: remon
 */

#ifndef NEWTONPOLYNOMIAL_H_
#define NEWTONPOLYNOMIAL_H_

#include "Polynomial.h"
#include "GlobalMacros.h"

class NewtonPolynomial: public Polynomial {
private:
	list<double> interpolationXs;
public:
	NewtonPolynomial(list<double> = list<double>());
	virtual ~NewtonPolynomial();

	list<Point> evaluatePoints(list<double> x_coordinates);
	Point evaluatePoint(double x);
};

#endif /* NEWTONPOLYNOMIAL_H_ */
