/*
 * LinearEquation.h
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#pragma once

#include "GlobalMacros.h"

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <list>

using namespace std;


//Represents an equation of the form a_1*x_1+a_2*x2 +... = b
class LinearEquation {

protected:
	double right_hand_side;
	vector<double> coefficients;
	vector<double> features; //unknowns
	vector<string> featureNames; //unknown names

public:
	LinearEquation(vector<double> coefficients = vector<double>(),
			vector<double> features = vector<double>(),
			double right_hand_side_value = 0);
	virtual ~LinearEquation();

	double inline get_right_hand_side() {return right_hand_side;};
	vector<double>  inline get_coefficients() {return coefficients;};
	vector<double>  inline get_features() {return features;};
	vector<string>  inline get_featureNames() {return featureNames;};

	void addCoefficient(double cofficient);
	void addFeature(double feature);
	void addUnknown(string name);

	void setCoefficient(double value, int idx);
	void setFeature(double value, int idx);

	double getCoefficient(int idx);
	double getFeature(int idx);

	double getFeatureCoefficientByName(string name);

	double getRightHandSide();
	double getErrorSquared();
	void setRightHandSide(double value);
	
	void evaluateRightHandSide();
	double evaluateLeftHandSide();
	string getStringRepresentation();
	//LinearEquation operator-(const LinearEquation& secondEquation);
	friend LinearEquation operator-(const LinearEquation&, const LinearEquation&);

	void setFromAugmentedMatrixRow(const vector<vector<double>> &matrix, int row_idx);

	vector<double> asAugmentedMatrixRow(map<string, int> unknownIndices);
	vector<double> asAugmentedMatrixRowSimple();
	virtual  PointXd evaluatePoint(PointXd, bool addbias = false);
	list<PointXd> evaluatePoints(list<PointXd>, bool addbias = false);

	void __dbgPrintUnsolved();
	void __dbgSetRandomCoefficients(int features_count);
};

LinearEquation operator-(const LinearEquation&, const LinearEquation&);
