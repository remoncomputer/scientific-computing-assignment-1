/*
 * NewtonInterpolationEquationSolver.cpp
 *
 *  Created on: Nov 24, 2018
 *      Author: remon
 */

#include "NewtonInterpolationEquationSolver.h"

#include "Matrix.h"

NewtonInterpolationEquationSolver::NewtonInterpolationEquationSolver() {
	// TODO Auto-generated constructor stub

}

NewtonInterpolationEquationSolver::~NewtonInterpolationEquationSolver() {
	// TODO Auto-generated destructor stub
}

NewtonPolynomial NewtonInterpolationEquationSolver::Solve(list<Point> points){
	//Solving of the coefficients using a table
	int n = points.size();
	Matrix<double> table(n, n, 0.0);
	list<double> x_s;
	auto lstItr = points.begin();
	for(int i = 0; i < n; i++){
		//x_s[i] = get<0>(*lstItr);
		x_s.push_back((*lstItr)[0]);
		table[0][i] = (*lstItr)[1];
		++lstItr;
	}
	vector<double> x_s_vector(x_s.begin(), x_s.end());
	double delta_y, delta_x;
	for(int i = 1; i < n; i++){
		for(int j = 0; j < n - i; j++){
			delta_y = table[i - 1][j + 1] - table[i - 1][j];
			delta_x = x_s_vector[i + j] - x_s_vector[j];// Make sure that this is right as I was sleeping when writing this
			table[i][j] = delta_y / delta_x;
		}
	}
	NewtonPolynomial outputPolynomial(x_s);
	double coefficient;
	for(int i = 0; i < n; i++){
		coefficient = table[i][0];
		outputPolynomial.addCoefficient(coefficient);
	}
	return outputPolynomial;
}
