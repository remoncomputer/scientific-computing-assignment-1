/*
 * GaussEquationSolver.h
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#ifndef ABSTRACTEQUATIONSOLVER_H_
#define ABSTRACTEQUATIONSOLVER_H_

#include <iostream>
#include <vector>
#include <map>

#include "GlobalMacros.h"
#include "LinearEquation.h"

#include <chrono>
using namespace chrono;

#define SOLVER_TYPE_GAUSS        1
#define SOLVER_TYPE_GAUSS_SEIDEL 2

using namespace std;
class AbstractEquationSolver {
protected:
	string title;
	string solver_type_name;
	vector<LinearEquation> equations;
	vector<double> unknowns;
	map<string, int> unknownIndices;
	int next_unknown_feature_index;
	milliseconds solve_start_time;
	milliseconds solve_end_time;
public:
	AbstractEquationSolver();
	void __dbgPrintMatrix(vector<vector<double>> &matrix);

		void setFromAugmentedMatrix(
				const vector<vector<double>> &matrix);

		vector<vector<double>> asAugmentedMatrix();

		void __dbgPrintUnsolved(string title);

		void __dbgSetRandomSystem(
				int equations_count, int features_count);

		vector<double> getUnknowns();

		void addEquation(LinearEquation equation);
		void addEquations(vector<LinearEquation> equation);

		map<string, double> getSolution();

		void onSolverStarted();
		virtual void solveEquations() = 0;
		void onSolveFinished();
		virtual ~AbstractEquationSolver();

		double getTotalError();
};

#endif /* ABSTRACTEQUATIONSOLVER_H_ */
