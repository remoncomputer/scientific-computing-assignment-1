/*
 * GaussEquationSolver.cpp
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#include "GaussEquationSolver.h"

#include <set>
#include <string>
#include <list>
#include <vector>

/*#include <Eigen/Dense>
using namespace Eigen;*/

GaussEquationSolver::GaussEquationSolver(string a_title, vector<LinearEquation> equations)  {
	this->title = a_title;
	this->solver_type_name = "Gaussian Elimination Solver";

	set<string> uniqueFeatureNames;
	vector<string> currentFeatureNames;
	for(LinearEquation eq: equations){
		currentFeatureNames = eq.get_featureNames();
		uniqueFeatureNames.insert(currentFeatureNames.begin(), currentFeatureNames.end());
	}
	//initialize unknownIndices
	int idx = 0;
	for(string featureName: uniqueFeatureNames){
		this->unknownIndices[featureName] = idx++;
	}
	this->next_unknown_feature_index = idx;
	//throw "Please handle the added unknown names";
	this->equations = equations;

}

GaussEquationSolver::~GaussEquationSolver() {
	// TODO Auto-generated destructor stub
}

void GaussEquationSolver::backwardSubstitute(const vector<vector<double>> &matrix) {
	vector<double> features;
	
	//TODO: should throw an exception if the system doesn't have a unique solution
	
	for(int r = matrix.size() - 1; r > -1; r--) {
		//cout << "Now in row: " << r << endl;
		
		// RHS sum is initially set to the rightmost value in the matrix
		double rhs_sum = matrix[r][matrix[r].size() - 1];
		//cout << "\trhs = " << rhs_sum << endl;

		// move all the Xs to the other side, except the X we want to
		// find. the Xs to the left are ignored, since their coefficients
		// are zeros. also notice that the last value in the row is not a
		// coefficient, so it was omitted now (and handled earlier)

		for(int i = r + 1; i < matrix[r].size() - 1; i++) {
			//cout << "\trhs_sum -= " << matrix[r][i] << " * " << features[i - (r + 1)] << endl;
			//cout << "\trhs_sum -= " << matrix[r][i] << " * " << "features[" << i - (r + 1) << "]" << endl;
			rhs_sum -= matrix[r][i] * features[i - (r + 1)];
		}

		// equivalent to dividing both sides by the coefficient
		// of the variable we want to find
		rhs_sum /= matrix[r][r];
		features.insert(features.begin(), rhs_sum);
	}

	for(int f = 0; f < features.size(); f++) {
		unknowns.push_back(features[f]);
	}

	for(int e = 0; e < equations.size(); e++) {
		for(int f = 0; f < features.size(); f++) {
			equations[e].addFeature(features[f]);
		}
	}
}

void GaussEquationSolver::rowReduce(vector<vector<double>> &matrix, int start_row_idx, int start_col_idx) {

	//__dbgPrintMatrix(matrix);

	// STEP 1: select the left-most non-zero column
	int left_most_nonzero_col_idx = -1;

	for(int c = start_col_idx; c < matrix[0].size(); c++) {
		for(int r = start_row_idx; r < matrix.size(); r++) {
			// TODO: should extremly small values be treated as zeros?
			if(!(matrix[r][c] <= 0.00000001 && matrix[r][c] >= -0.00000001)) {
				left_most_nonzero_col_idx = c;
				break;
			}
		}

		// non-initial value means that we found the column
		if(left_most_nonzero_col_idx != -1) {
			break;
		}
	}
	//cout << "left_most_nonzero_col_idx: " << left_most_nonzero_col_idx << endl;

	// if the search is over and we still haven't found a non-zero column
	if(left_most_nonzero_col_idx == -1) {
		// then the matrix is a zero matrix and is, therefore, in reduced echeleon form
		return;
	}

	// STEP 2.1: select the largest absloute value in the column
	double max_abs = -1;
	double max_abs_row = -1;
	for(int r = start_row_idx; r < matrix.size(); r++) {
		double current_abs = abs(matrix[r][left_most_nonzero_col_idx]);
		if(current_abs > max_abs) {
			max_abs = current_abs;
			max_abs_row = r;
		}
	}

	if(max_abs < 0) {
		throw "internal error: couldn't set max_abs";
	}

	if(max_abs == 0) {
		throw "internal error: max_abs can't be zero";
	}

	if(max_abs_row < 0) {
		throw "internal error: couldn't set max_abs_row";
	}
	
	//cout << "max_abs_row: " << max_abs_row << endl;
	// STEP 2.2: interchange if necessary
	if(max_abs_row != start_row_idx) {
		for(int c = start_col_idx; c < matrix[0].size(); c++) {
			double temp = matrix[max_abs_row][c];
			matrix[max_abs_row][c] = matrix[start_row_idx][c];
			matrix[start_row_idx][c] = temp;
		}
	}

	// STEP 3: create zeroes below the pivot

	// just renaming older variables. hopefully the compiler will optimize this out
	int pivot_row = start_row_idx;
	int pivot_col = left_most_nonzero_col_idx;
	
	for(int r = pivot_row + 1; r < matrix.size(); r++) {
		double factor_numerator = -matrix[r][pivot_col];
		double factor_denominator = matrix[pivot_row][pivot_col];

		//TODO: should we divide both the numerator and the denominator by the greatest common divisor?

		for(int c = pivot_col; c < matrix[r].size(); c++) {
			if(c == pivot_col) {
				// TODO: unexpected behaviour when using -0 instead of 0
				matrix[r][c] = 0;
			}
			else {
				matrix[r][c] += (matrix[pivot_row][c] * factor_numerator) / factor_denominator;
			}
			//cout << matrix[r][c] << " ";
		}
		//cout << endl;
	}
	//cout << endl;

	// STEP 4: cover the row of the current pivot and repeat
	rowReduce(matrix, pivot_row + 1, pivot_col);
}

void GaussEquationSolver::solveEquations() {
	this->onSolverStarted();
	//__dbgPrintUnsolved("test");
	vector<vector<double>> augmented = asAugmentedMatrix();
//	if(useEign){
/*		int numEquations = augmented.size();
		MatrixXd A(numEquations,numEquations);
		VectorXd B(numEquations);
		for(int colIdx = 0; colIdx < numEquations; colIdx++){
			vector<double> row = augmented[colIdx];
			for(int rowIdx = 0; rowIdx < numEquations; rowIdx++){
				A(colIdx, rowIdx) = row[rowIdx];
			}
			B[colIdx] = row[numEquations];
		}
		VectorXd X = A.colPivHouseholderQr().solve(B);//A.inverse() * B;
		this->unknowns = vector<double>();
		for(int idx = 0; idx < numEquations; idx++){
			this->unknowns.push_back(X[idx]);
		}
		cout << "Here" << endl;*/
//	}else{
		rowReduce(augmented, 0, 0);
		backwardSubstitute(augmented);
//	}
	this->onSolveFinished();
}
