/*
 * LinearEquationWithKernel.h
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#ifndef LINEAREQUATIONWITHKERNEL_H_
#define LINEAREQUATIONWITHKERNEL_H_

#include "LinearEquation.h"
#include "Kernel.h"

#include <memory>

class LinearEquationWithKernel: public LinearEquation {
private:
	shared_ptr<Kernel> kernel;
public:
	LinearEquationWithKernel();
	virtual ~LinearEquationWithKernel();

	void setKernel(shared_ptr<Kernel> kernel){
		this->kernel = kernel;
	}

	virtual Point evaluatePoint(Point, bool addbias = true);
	string getStringRepresentation();
};

#endif /* LINEAREQUATIONWITHKERNEL_H_ */
