/*
 * PolynomialRegressionSolver.cpp
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#include "PolynomialRegressionSolver.h"
#include "Utils.h"

PolynomialRegressionSolver::PolynomialRegressionSolver(int polynomialDegree) {
	// TODO Auto-generated constructor stub
	this->polynomialDegree = polynomialDegree;
}

PolynomialRegressionSolver::~PolynomialRegressionSolver() {
	// TODO Auto-generated destructor stub
}

Polynomial PolynomialRegressionSolver::solve(list<Point> points, int solver_type){
	//double sum_y_x_vals[this->polynomialDegree](0); //will this initialize values to 0
	double *sum_y_x_vals, *sum_x_x_vals;
	Utils::initializeArray(sum_y_x_vals, this->polynomialDegree + 1, 0.0);
	//double sum_x_x_vals[this->polynomialDegree + 2](0); //will this initialize values to 0
	Utils::initializeArray(sum_x_x_vals, this->polynomialDegree + 3, 0.0);

	//for(int i = 0; )
	double x, y, x_power_x_accumulator;
	int idx;
	for(Point p: points){
		//x = p[0];
		x = p[0];
		//y = p[1];
		y = p[1];
		idx = 0;
		x_power_x_accumulator = 1;
		for(; idx < this->polynomialDegree + 1; idx++){
			sum_y_x_vals[idx] += y * x_power_x_accumulator;
			sum_x_x_vals[idx] += x_power_x_accumulator;
			x_power_x_accumulator *= x;
		}
		for(; idx < this->polynomialDegree + 3; idx++){
			sum_x_x_vals[idx] += x_power_x_accumulator;
			x_power_x_accumulator *= x;
		}
	}

	LinearEquation equation_i;
	int featureCounter;
	string feature_name;

	// create empty solvers
	GaussEquationSolver gauss_solver("Polynomial Regression Problem");
	GaussSeidelEquationSolver gauss_seidel_solver("Polynomial Regression Problem");

	// choose one of them
	AbstractEquationSolver *solver;
	if(solver_type == SOLVER_TYPE_GAUSS) {
		solver = &gauss_solver;
	}
	else if(solver_type == SOLVER_TYPE_GAUSS_SEIDEL) {
		solver = &gauss_seidel_solver;
	}



	for(int i=0; i < this->polynomialDegree + 1; i++){
		equation_i = LinearEquation();
		equation_i.setRightHandSide(sum_y_x_vals[i]);
		featureCounter = 0;
		for(int j = i; j < i + this->polynomialDegree + 1; j++){
			feature_name = "a" + to_string(featureCounter++);
			equation_i.addUnknown(feature_name);
			equation_i.addCoefficient(sum_x_x_vals[j]);
		}
		//equation_i.prepareUnderlingEquationForSolver();
		solver->addEquation(equation_i);
	}
	solver->solveEquations();
	vector<double> outputPolynomialCoefficients = solver->getUnknowns();
	Polynomial outputPolynomial;
	for(double coefficient:outputPolynomialCoefficients){
		outputPolynomial.addCoefficient(coefficient);
	}

	delete[] sum_x_x_vals;
	delete[] sum_y_x_vals;
	return outputPolynomial;
}

