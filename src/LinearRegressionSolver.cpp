/*
 * LinearRegressionSolver.cpp
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#include "LinearRegressionSolver.h"

LinearRegressionSolver::LinearRegressionSolver() {
	// TODO Auto-generated constructor stub

}

LinearRegressionSolver::~LinearRegressionSolver() {
	// TODO Auto-generated destructor stub
}

Polynomial LinearRegressionSolver::solve(list<Point> points){
	double sumx = 0, sumxy =0, sumy = 0, sumx2 = 0;
	int n = points.size();
	double x, y;
	for(Point p: points){
		//x = p[0];
		x = p[0];
		//y = p[1];
		y = p[1];
		sumx += x;
		sumxy += x * y;
		sumy += y;
		sumx2 += x * x;
	}
	double xm = sumx / n;
	double ym = sumy / n;
	double a1 = (n * sumxy - sumx * sumy) / (n * sumx2 - sumx * sumx);
	double a0 = ym - a1 * xm;

	Polynomial output;
	output.addCoefficient(a0);
	output.addCoefficient(a1);
	return output;
}
