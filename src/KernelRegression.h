/*
 * MultiplePolynomialRegression.h
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#ifndef KERNELREGRESSION_H_
#define KERNELREGRESSION_H_

#include "GlobalMacros.h"
#include "PolynomialRegressionSolver.h"
#include "Kernel.h"
#include "LinearEquationWithKernel.h"

#include <memory>

class KernelRegression: public PolynomialRegressionSolver {
private:
	shared_ptr<Kernel> kernel;
public:
	KernelRegression();
	virtual ~KernelRegression();
	void setKernel(shared_ptr<Kernel> k){
		this->kernel = k;
	}
	LinearEquationWithKernel solve(list<Point> points, int solver_type);
};

#endif /* KERNELREGRESSION_H_ */
