/*
 * Polynomial.h
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#ifndef POLYNOMIAL_H_
#define POLYNOMIAL_H_

#include "GlobalMacros.h"
#include "LinearEquation.h"
#include <tuple>
#include <list>

class Polynomial: public LinearEquation{
private:
	double x;
	bool equation_ready_for_solver;
public:
	Polynomial(double = 0);
	Polynomial(LinearEquation);
	virtual ~Polynomial();

	void evaluateRightHandSide();
	void setx(double);
	Point evaluatePoint(double x);
	list<Point> evaluatePoints(list<double> x_coordinates);
	Polynomial diffrentiate();
	void prepareUnderlingEquationForSolver();
	Polynomial operator-(Polynomial& secondEquation);
	string get_string_representation();
	};

#endif /* POLYNOMIAL_H_ */
