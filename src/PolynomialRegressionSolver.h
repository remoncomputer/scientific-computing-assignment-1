/*
 * PolynomialRegressionSolver.h
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#ifndef POLYNOMIALREGRESSIONSOLVER_H_
#define POLYNOMIALREGRESSIONSOLVER_H_

#include "GaussEquationSolver.h"
#include "GaussSeidelEquationSolver.h"
#include "AbstractEquationSolver.h"
#include "Polynomial.h"
#include "GlobalMacros.h"
#include "LinearRegressionSolver.h"

#include <tuple>
#include <list>


class PolynomialRegressionSolver: public LinearRegressionSolver {
private:
	int polynomialDegree;
public:
	PolynomialRegressionSolver(int polynomialDegree = 2);
	virtual ~PolynomialRegressionSolver();
	Polynomial solve(list<Point> points, int solver_type);
};

#endif /* POLYNOMIALREGRESSIONSOLVER_H_ */
