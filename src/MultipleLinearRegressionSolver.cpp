/*
 * MultipleLinearRegressionSolver.cpp
 *
 *  Created on: Dec 3, 2018
 *      Author: remon
 */

#include "MultipleLinearRegressionSolver.h"
#include "Matrix.h"
#include "GaussEquationSolver.h"
#include "GaussSeidelEquationSolver.h"

MultipleLinearRegressionSolver::MultipleLinearRegressionSolver() {
	// TODO Auto-generated constructor stub

}

MultipleLinearRegressionSolver::~MultipleLinearRegressionSolver() {
	// TODO Auto-generated destructor stub
}

LinearEquation MultipleLinearRegressionSolver::solve(list<PointXd> points, int solver_type){
	int n = points.size();
	double sumx1 = 0, sumx2 = 0, sumx1Square = 0, sumx2Square = 0, sumx1x2 = 0;
	double sumy = 0, sumx1y = 0, sumx2y = 0;
	double x1, x2, y;
 	for(PointXd p:points){
 		x1 = p[0];
 		x2 = p[1];
 		y = p[2];

 		sumx1 += x1;
 		sumx2 += x2;
 		sumx1Square += x1 * x1;
 		sumx2Square += x2 * x2;
 		sumx1x2 += x1 * x2;

 		sumy += y;
 		sumx1y += x1 * y;
 		sumx2y += x2 * y;
	}

 	Matrix<double> coefficients(3,4, 0);

 	coefficients[0][0] = n;
 	coefficients[1][1] = sumx1Square;
 	coefficients[2][2] = sumx1Square;

 	coefficients[0][1] = sumx1;
 	coefficients[1][0] = sumx1;
 	coefficients[0][2] = sumx2;
 	coefficients[2][0] = sumx2;

 	coefficients[1][2] = sumx1x2;
 	coefficients[2][1] = sumx1x2;

 	coefficients[0][3] = sumy;
 	coefficients[1][3] = sumx1y;
 	coefficients[2][3] = sumx2y;

 	string variableName [] = {"a0", "a1", "a2"};
 	vector<LinearEquation> equations;
 	LinearEquation currentEquation;
 	string unknownName;
 	for(int i = 0; i < 3; i++){
 		currentEquation = LinearEquation();
 		for(int j = 0; j < 3; j++){
 			unknownName = variableName[j];
 			currentEquation.addUnknown(unknownName);
 			currentEquation.addCoefficient(coefficients[i][j]);
 		}
 		currentEquation.setRightHandSide(coefficients[i][3]);
 		equations.push_back(currentEquation);
 	}
	// create empty solvers
	GaussEquationSolver gauss_solver("Multiple Regression Problem");
	GaussSeidelEquationSolver gauss_seidel_solver("Multiple Regression Problem");

	// choose one of them
	AbstractEquationSolver *solver;
	if(solver_type == SOLVER_TYPE_GAUSS) {
		solver = &gauss_solver;
	}
	else if(solver_type == SOLVER_TYPE_GAUSS_SEIDEL) {
		solver = &gauss_seidel_solver;
	}
	solver->addEquations(equations);

 	solver->solveEquations();
 	map<string, double> solution = solver->getSolution();
 	LinearEquation outputEquation;
 	for(auto entry:solution){
 		outputEquation.addCoefficient(entry.second);
 	}
 	outputEquation.addUnknown("1");
 	string basicFeatureName = "X";
 	for(int featureCounter = 0; featureCounter < sizeof(variableName) - 1; ++featureCounter){
 		outputEquation.addUnknown(basicFeatureName + to_string(featureCounter));
 	}
 	return outputEquation;
}
