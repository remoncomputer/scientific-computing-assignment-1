/*
 * Utils.cpp
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#include "Utils.h"

#include <fstream>

#include "gnuplot-iostream.h"

Utils::Utils() {
	// TODO Auto-generated constructor stub

}

Utils::~Utils() {
	// TODO Auto-generated destructor stub
}

list<Point> Utils::loadPointsFromFile(string filepath){
	//Output points
	list<Point> points;
	//The file that we will open
	ifstream file_stream(filepath);
	double x,y;
	string tokenOrLine;
	char separator = ',';
	Point currentPoint;
	if(file_stream.is_open()){
		getline(file_stream, tokenOrLine);// Reading the header
		while(!file_stream.eof()){
			//file_stream >> x >> y;
			getline(file_stream, tokenOrLine, separator);
			//cout << "{"<< tokenOrLine << "}" << endl;
			if(tokenOrLine == ""){
				continue;
			}
			x = stod(tokenOrLine);
			getline(file_stream, tokenOrLine);
			//cout << "{"<< tokenOrLine << "}" << endl;
			if(tokenOrLine == ""){
				continue;
			}
			y = stod(tokenOrLine);
			currentPoint = Point();
			currentPoint.push_back(x);
			currentPoint.push_back(y);
			points.push_back(currentPoint);
		}
		file_stream.close();
	}
	else {
		cout << "ERROR: Filed to open stream: " << filepath << endl;
	}
	return points;
}

list<PointXd> Utils::loadPointsXdFromFile(string filePath, int dim){
	//Output points
		list<PointXd> points;
		//The file that we will open
		ifstream file_stream(filePath);
		string tokenOrLine;
		char separator = ',';
		PointXd currentPoint;
		if(file_stream.is_open()){
			getline(file_stream, tokenOrLine);//removing the header
			while(!file_stream.eof()){
				currentPoint = PointXd();
				for(int i = 0; i < dim - 1; i++){
					getline(file_stream, tokenOrLine, separator);
					//cout << "{"<< tokenOrLine << "}" << endl;
					//cout << "Token: " << tokenOrLine << endl;
					if(tokenOrLine == ""){
						continue;
					}
					currentPoint.push_back(stod(tokenOrLine));
				}
				getline(file_stream, tokenOrLine);
				if(tokenOrLine == ""){
					continue;
				}
				//cout << "Token: " << tokenOrLine << endl;
				currentPoint.push_back(stod(tokenOrLine));
				points.push_back(currentPoint);
			}
			file_stream.close();
		}
		else {
			cout << "ERROR: Filed to open stream: " << filePath << endl;
		}
		return points;
}

list<double> Utils::x_axis_multiplier(list<double> x_coordinates, int multiplication_factor){
	list<double> output_coordinates;
	double lower_boundary, upper_boundary, delta;
	//x_coordinates.sort();
	lower_boundary = *min_element(x_coordinates.begin(), x_coordinates.end());
	upper_boundary = *max_element(x_coordinates.begin(), x_coordinates.end());
	int num_points = x_coordinates.size();
	int number_of_distances = num_points *multiplication_factor -1;
	delta = (upper_boundary - lower_boundary) / number_of_distances;
	for(int idx = 0; idx <= number_of_distances; ++idx){
		output_coordinates.push_back(lower_boundary +  delta * idx);
	}
	/*auto corrdinate_itr = x_coordinates.begin();
	output_coordinates.push_back(*corrdinate_itr);
	double current_x_value;
	for(; corrdinate_itr != x_coordinates.end() ;){
		lower_boundary = *corrdinate_itr;
		++corrdinate_itr;
		if(corrdinate_itr == x_coordinates.end()){
			break;
		}
		upper_boundary = *corrdinate_itr;
		delta = (upper_boundary - lower_boundary) / multiplication_factor;
		for(int counter = 1; counter <= multiplication_factor; ++counter){
			current_x_value = lower_boundary + counter * delta;
			output_coordinates.push_back(current_x_value);
		}
		//output_coordinates.push_back(upper_boundary);
	}*/
	return output_coordinates;
}

	PointXd Utils::getMinPoint(list<PointXd> points){
		auto pointItr = points.begin();
		PointXd minPoint = clonePoint(*pointItr);
		int dim = pointItr->size();
		++pointItr;
		for(; pointItr != points.end(); ++pointItr){
			for(int dimIdx = 0; dimIdx < dim; ++dimIdx){
				if((*pointItr)[dimIdx] < minPoint[dimIdx]){
					minPoint[dimIdx] = (*pointItr)[dimIdx];
				}
			}
		}
		return minPoint;
	}

	PointXd Utils::getMaxPoint(list<PointXd> points){
		auto pointItr = points.begin();
		PointXd maxPoint = clonePoint(*pointItr);
		int dim = pointItr->size();
		++pointItr;
		for(; pointItr != points.end(); ++pointItr){
			for(int dimIdx = 0; dimIdx < dim; ++dimIdx){
				if((*pointItr)[dimIdx] > maxPoint[dimIdx]){
					maxPoint[dimIdx] = (*pointItr)[dimIdx];
				}
			}
		}
		return maxPoint;
	}

	PointXd Utils::getDeltaPoint(int stepsPerDim, PointXd minPoint, PointXd maxPoint){
		PointXd delta;
		if(minPoint.size() != maxPoint.size()){
			throw "min and max points must be of the same dimension";
		}
		auto minItr = minPoint.begin();
		auto maxItr = maxPoint.begin();
		for(; minItr != minPoint.end() && maxItr != maxPoint.end(); ++minItr, ++maxItr){
			delta.push_back((*maxItr - *maxItr)/stepsPerDim);
		}
		return delta;
	}

	list<list<double>> Utils::makeGridCoordinates(PointXd minPoint, PointXd maxPoint, PointXd delta){
		list<list<double>> gridCoordinates;
		auto minPointItr = minPoint.begin();
		auto maxPointItr = maxPoint.begin();
		auto deltaItr = delta.begin();
		list<double> currentCoordinatePoints;
		for(; minPointItr != minPoint.end() && maxPointItr != maxPoint.end() &&
			deltaItr != delta.end(); ++minPointItr, ++maxPointItr, ++deltaItr){
			currentCoordinatePoints = list<double>();
			for(double currentPoint = *minPointItr; currentPoint < *maxPointItr; currentPoint += *deltaItr){
				currentCoordinatePoints.push_back(currentPoint);
			}
			gridCoordinates.push_back(currentCoordinatePoints);
		}
		return gridCoordinates;
	}

	list<PointXd> Utils::makeGrid(list<list<double>> gridCoodinates){
		list<PointXd> outputPoints;
		auto gridCoordinatesItr = gridCoodinates.begin();
		list<PointXd> tmpPoints;
		PointXd currentPoint;
		auto gridPointItr = gridCoordinatesItr->begin();
		for(;gridPointItr != gridCoordinatesItr->end(); ++gridPointItr){
			currentPoint = PointXd();
			currentPoint.push_back(*gridPointItr);
			tmpPoints.push_back(currentPoint);
		}
		++gridCoordinatesItr;
		PointXd newPoint;
		for(; gridCoordinatesItr != gridCoodinates.end(); ++gridCoordinatesItr){
			outputPoints = list<PointXd>();
			for(auto tmpPointsItr = tmpPoints.begin(); tmpPointsItr != tmpPoints.end();
					++tmpPointsItr){
				for(gridPointItr = gridCoordinatesItr->begin();
						gridPointItr != gridCoordinatesItr->end(); ++gridPointItr){

						currentPoint = *tmpPointsItr;
						newPoint = clonePoint(currentPoint);
						newPoint.push_back(*gridPointItr);
						outputPoints.push_back(newPoint);
				}
			}
			tmpPoints = outputPoints;
		}
		outputPoints = tmpPoints;
		return outputPoints;
	}

	PointXd Utils::clonePoint(PointXd originalPoint){
		return PointXd(originalPoint.begin(), originalPoint.end());
	}

list<PointXd> Utils::nDimensionPointMultiplier(list<PointXd> points, int multipilcationFactorPerDimension){
	list<PointXd> outputPoints;
	//int dim = points.begin()->size();
	PointXd minPoint = getMinPoint(points);
	PointXd maxPoint = getMaxPoint(points);
	PointXd deltaPoint = getDeltaPoint(multipilcationFactorPerDimension, minPoint, maxPoint);
	list<list<double>> gridCoodinates = makeGridCoordinates(minPoint, maxPoint, deltaPoint);
	outputPoints = makeGrid(gridCoodinates);
	return outputPoints;
}

void Utils::plotPoints(list<Point> points){
	Gnuplot gp;
	//gp << "set xrange [-2:20]\nset yrange [-2:50]\n";
	gp << "plot '-' with points title 'points' lt rgb 'red' \n"; //lt rgb 'violet' //lines //, '-' with vectors title 'pts_B'
	//"set style data points" //To plot data points
	//vector<tuple<double, double>> pointsVector(points.begin(), points.end());
	gp.send1d(points);
}
