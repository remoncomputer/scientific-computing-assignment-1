/*
 * MultipleFeaturesPolynomial.cpp
 *
 *  Created on: Dec 3, 2018
 *      Author: remon
 */

#include "MultipleFeaturesPolynomial.h"

MultipleFeaturesPolynomial::MultipleFeaturesPolynomial() {
	// TODO Auto-generated constructor stub

}

MultipleFeaturesPolynomial::MultipleFeaturesPolynomial(double x1, double x2): x1(x1), x2(x2){

}

MultipleFeaturesPolynomial::~MultipleFeaturesPolynomial() {
	// TODO Auto-generated destructor stub
}

void MultipleFeaturesPolynomial::evaluateRightHandSide(){

}

void MultipleFeaturesPolynomial::setx1(double x1){
	this->x1 = x1;
}

void MultipleFeaturesPolynomial::setx2(double x2){
	this->x2 = x2;
}

/*list<double> MultipleFeaturesPolynomial::getKernals(Point3d p){
	list<double> kernals;
	double x1 = get<0>(p);
	double x2 = get<1>(p);
	kernals.push_back(1);
	kernals.push_back(x1);
	kernals.push_back(x2);
	//and continue
}*/

Point MultipleFeaturesPolynomial::evaluatePoint(double x){

}

list<Point> MultipleFeaturesPolynomial::evaluatePoints(list<double> x_coordinates){
	list<Point> outputPoints;
	for(double x:x_coordinates){
		outputPoints.push_back(this->evaluatePoint(x));
	}
	return outputPoints;
}

string MultipleFeaturesPolynomial::get_string_representation(){

}
void MultipleFeaturesPolynomial::setKernel(shared_ptr<Kernel> k){
	this->kernel = k;
}
