/*
 * MultipleFeaturesPolynomial.h
 *
 *  Created on: Dec 3, 2018
 *      Author: remon
 */

#ifndef MULTIPLEFEATURESPOLYNOMIAL_H_
#define MULTIPLEFEATURESPOLYNOMIAL_H_

#include "GlobalMacros.h"
#include "Kernel.h"

#include <memory>
#include <list>

class MultipleFeaturesPolynomial {
private:
	double x1;
	double x2;
	shared_ptr<Kernel> kernel;
public:
	MultipleFeaturesPolynomial(double = 0, double = 0);
	MultipleFeaturesPolynomial();
	virtual ~MultipleFeaturesPolynomial();

	void evaluateRightHandSide();
	void setx1(double);
	void setx2(double);
	Point evaluatePoint(double x);
	list<Point> evaluatePoints(list<double> x_coordinates);
	string get_string_representation();
	void setKernel(shared_ptr<Kernel> k);
	//list<double> getKernals(Point3d p);
};

#endif /* MULTIPLEFEATURESPOLYNOMIAL_H_ */
