/*
 * Utils.h
 *
 *  Created on: Nov 23, 2018
 *      Author: remon
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <tuple>
#include <list>

#include "GlobalMacros.h"

using namespace std;
// This is a static class for utilities
class Utils {
private:
	Utils();
public:
	~Utils();

	static list<Point> loadPointsFromFile(string filepath);
	static list<PointXd> loadPointsXdFromFile(string filePath, int dim);
	static void plotPoints(list<Point> points);

	template <class T>
	static void initializeArray(T* &array, int size, T initialValue){
		array = new T[size];
		for(int i = 0; i < size; i++){
			array[i] = initialValue;
		}
	}

	static list<double> x_axis_multiplier(list<double> x_coordinates, int multiplication_factor);
	static list<PointXd> nDimensionPointMultiplier(list<PointXd> points, int multipilcationFactorPerDimension);
	static PointXd getMinPoint(list<PointXd> points);
	static PointXd getMaxPoint(list<PointXd> points);
	static PointXd getDeltaPoint(int stepsPerDim, PointXd minPoint, PointXd maxPoint);
	static list<list<double>> makeGridCoordinates(PointXd minPoint, PointXd maxPoint, PointXd delta);
	static list<PointXd> makeGrid(list<list<double>> gridCoodinates);
	static PointXd clonePoint(PointXd originalPoint);
};

#endif /* UTILS_H_ */
