/*
 * AbstractEquationSolver.cpp
 *
 *  Created on: Dec 5, 2018
 *      Author: remon
 */

#include "AbstractEquationSolver.h"

AbstractEquationSolver::AbstractEquationSolver(){
	this->next_unknown_feature_index = 0;
}
void AbstractEquationSolver::__dbgPrintMatrix(vector<vector<double>> &matrix) {
	for (int r = 0; r < matrix.size(); r++) {
		for (int c = 0; c < matrix[r].size(); c++) {
			cout << matrix[r][c] << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void AbstractEquationSolver::setFromAugmentedMatrix(
		const vector<vector<double>> &matrix) {

	for (int row_idx = 0; row_idx < matrix.size(); row_idx++) {
		LinearEquation eq;
		eq.setFromAugmentedMatrixRow(matrix, row_idx);
		equations.push_back(eq);
	}
}

vector<vector<double>> AbstractEquationSolver::asAugmentedMatrix() {
	vector<vector<double>> matrix;

	for (int i = 0; i < equations.size(); i++) {
		matrix.push_back(
				equations[i].asAugmentedMatrixRow(this->unknownIndices));
	}

	return matrix;
}

void AbstractEquationSolver::__dbgPrintUnsolved(string title) {
	cout << title << endl;

	for (int i = 0; i < equations.size(); i++) {
		equations[i].__dbgPrintUnsolved();
	}
}

void AbstractEquationSolver::__dbgSetRandomSystem(int equations_count, int features_count) {

	for (int i = 0; i < equations_count; i++) {
		LinearEquation eq;
		eq.__dbgSetRandomCoefficients(features_count);
		equations.push_back(eq);
	}
}

vector<double> AbstractEquationSolver::getUnknowns() {
	return unknowns;
}

void AbstractEquationSolver::addEquation(LinearEquation equation) {
	vector < string > currentEquationFeatureNames = equation.get_featureNames();
	for (string featureName : currentEquationFeatureNames) {
		if (this->unknownIndices.find(featureName)
				== this->unknownIndices.end()) {
			this->unknownIndices[featureName] =
					this->next_unknown_feature_index++;
		}
	}
	equations.push_back(equation);
}

void AbstractEquationSolver::addEquations(vector<LinearEquation> eqs) {
	for(int i = 0; i < eqs.size(); i++) {
		addEquation(eqs[i]);
	}
}


map<string, double> AbstractEquationSolver::getSolution() {
	map<string, double> solution;
	for (auto const& mapEntry : this->unknownIndices) {
		solution[mapEntry.first] = this->unknowns[mapEntry.second];
	}
	return solution;
}

void AbstractEquationSolver::onSolverStarted() {
	solve_start_time = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

	cout << "Title:       " << title << endl;
	cout << "Solver:      " << solver_type_name << endl;
	cout << "Started on:  " << solve_start_time.count() << endl;
}

void AbstractEquationSolver::onSolveFinished() {
	solve_end_time = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	cout << "Finished on: " << solve_end_time.count() << endl;
	cout << "Elapsed:     " << solve_end_time.count() - solve_start_time.count() << endl;
	cout << "Error:       " << getTotalError() << endl;
}

double AbstractEquationSolver::getTotalError() {
	double total_error = 0;
	for(int i = 0; i < equations.size(); i++) {
		total_error += equations[i].getErrorSquared();
	}

	return total_error;
}

AbstractEquationSolver::~AbstractEquationSolver(){

}
