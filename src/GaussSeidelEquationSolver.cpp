/*
 * GaussSeidelEquationSolver.cpp
 *
 *  Created on: Nov 20, 2018
 *      Author: remon
 */

#include "GaussSeidelEquationSolver.h"
#include <iostream>

using namespace std;

GaussSeidelEquationSolver::GaussSeidelEquationSolver(string a_title, vector<LinearEquation> equations) {
	this->title = a_title;
	this->solver_type_name = "Gauss Seidel Solver";

	this->equations = equations;
}

GaussSeidelEquationSolver::~GaussSeidelEquationSolver() {
	// TODO Auto-generated destructor stub
}

void printVector(const vector<double> &v) {
	for(int i = 0; i < v.size(); i++) {
		cout << " " << v[i] << endl;
	}
}

vector<vector<double>> GaussSeidelEquationSolver::asAugmentedMatrixSimple() {
	vector<vector<double>> matrix;
	
	for(int i = 0; i < equations.size(); i++) {
		matrix.push_back(equations[i].asAugmentedMatrixRow(unknownIndices));
	}

	return matrix;
}

void GaussSeidelEquationSolver::solveEquations() {
	this->onSolverStarted();
	vector<vector<double>> augmented = AbstractEquationSolver::asAugmentedMatrix();//asAugmentedMatrixSimple();

	vector <double> solution(equations.size());

	for(int i = 0; i < solution.size(); i++) {
		solution[i] = 0;
	}

	bool converged;
	int iteration_index = 0;
	int max_iterations = 50;
	do {
		// assume convergence. but unassume it later if an error is too large
		converged = true;

		// iterate through the variables
		for(int i = 0; i < solution.size(); i++) {
			double x_i = augmented[i][augmented[i].size() - 1];

			// iterate through the terms in the equation
			for(int j = 0; j < augmented.size(); j++) {
				if(i == j) {
					continue;
				}
				
				x_i -= augmented[i][j] * solution[j];
			}

			x_i /= augmented[i][i];
			double prev_x_i = solution[i];
			solution[i] = x_i;

			double error = 100 * (x_i - prev_x_i) / x_i;
			if(error > 0.0000001 || error < -0.0000001) {
				converged = false;
			}
		}

		iteration_index++;
	} while(!converged && iteration_index < max_iterations);
	
	for(int f = 0; f < solution.size(); f++) {
		unknowns.push_back(solution[f]);
	}

	for(int e = 0; e < equations.size(); e++) {
		for(int f = 0; f < solution.size(); f++) {
			equations[e].addFeature(solution[f]);
		}
	}
	this->onSolveFinished();
}

vector<double> GaussSeidelEquationSolver::getSolutionSimple() {
	return unknowns;
}
