/*
 * Kernel.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#include "Kernel.h"

Kernel::Kernel() {
	// TODO Auto-generated constructor stub

}

Kernel::~Kernel() {
	// TODO Auto-generated destructor stub
}

list<Point> Kernel::calculateKernel(list<Point> input){
	list<Point> outputPoints;
	for(auto p:input){
		outputPoints.push_back(this->calculateKernel(p));
	}
	return outputPoints;
}
