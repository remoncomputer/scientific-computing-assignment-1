/*
 * Kernel.h
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include "GlobalMacros.h"

#include <list>
#include <string>

class Kernel {
public:
	Kernel();
	virtual ~Kernel();

	virtual Point calculateKernel(Point) = 0;
	virtual vector<string> getStringRepresentation() = 0;
	virtual list<Point> calculateKernel(list<Point>);
};

#endif /* KERNEL_H_ */
