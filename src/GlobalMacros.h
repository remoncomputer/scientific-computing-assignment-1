#pragma once

#include <tuple>
#include <vector>

//#define Point tuple<double, double>
#define Point vector<double>
//#define Point3d tuple<double, double, double>
//#define PointXd vector<double>
#define PointXd Point

using namespace std;

#warning "Add Helmy Classes and New Classes for MultipleRegression (Linear and Polynomial) to CMake"
#warning "Add the above classes to Seidel test also"
