/*
 * LinearEquationWithKernel.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: remon
 */

#include "LinearEquationWithKernel.h"

LinearEquationWithKernel::LinearEquationWithKernel() {
	// TODO Auto-generated constructor stub

}

LinearEquationWithKernel::~LinearEquationWithKernel() {
	// TODO Auto-generated destructor stub
}

Point LinearEquationWithKernel::evaluatePoint(Point inputPoint, bool addbias){
	Point kernelOutput = this->kernel->calculateKernel(inputPoint);
	Point outputFromLinearEuation = LinearEquation::evaluatePoint(kernelOutput);
	Point output = Point(inputPoint.begin(), inputPoint.end());
	output.push_back(*outputFromLinearEuation.rbegin());
	return output;
}

string LinearEquationWithKernel::getStringRepresentation(){
	vector<string> kernelRepresentation = this->kernel->getStringRepresentation();
	string representation = "";
	auto kernelRepresentationItr = kernelRepresentation.begin();
	auto coefficentsItr = this->coefficients.begin();
	representation += to_string(*coefficentsItr) + "*" + *kernelRepresentationItr;
	++kernelRepresentationItr, ++coefficentsItr;
	string sign;
	for(;kernelRepresentationItr != kernelRepresentation.end() && coefficentsItr != this->coefficients.end();
			++kernelRepresentationItr, ++coefficentsItr){
		string sign = (*coefficentsItr >= 0)? "+":"";
		representation += sign + to_string(*coefficentsItr) + "*" + *kernelRepresentationItr;
	}
	return representation;
}
